package co.com.principal;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class StatictsString {
	private int  valorsuma = 1;
	private Map<String,Integer> values;

	public StatictsString() {
		
	}
	/**
	 * 
	 * @param cadena
	 * @return
	 */
	public String stringStatics(String cadena) {
		String response = "";
		values = new HashMap<String, Integer>();
		values =makeMap(cadena);
		int unicas = values.size();
		String maxString = strinMax(values);
		response ="resultado palabras unicas " +unicas+" mas repetida"+ maxString;
				
		return response;
	}
	
	/**
	 * 
	 * @param vals
	 * @return
	 */
	public Map<String,Integer> makeMap(String vals){
		Map<String,Integer> consolidate = new HashMap<String,Integer>();
		try {
			String[] values = vals.split(" ");
			for(String text :values) {
			  	if(consolidate.size()>0) {
			  		if(consolidate.containsKey(text)) {
			  		  int total = (int)consolidate.get(text);
			  		    consolidate.put(text, total+1);
			  		}else {
			  			consolidate.put(text,valorsuma);
			  		}
			  	}else {
			  		consolidate.put(text,valorsuma);
			  	}
			  	
			}
		}catch(Exception e) {
			throw e;
		}
		return consolidate;
	}
	
	public String strinMax(Map<String,Integer> consolidated) {
		String response = "";
		Entry<String,Integer> maxEntry = Collections.max(consolidated.entrySet(), (Entry<String,Integer> e1, Entry<String,Integer> e2) -> e1.getValue()
		        .compareTo(e2.getValue()));
		 response = maxEntry.getKey();
		return response;
	}
}
