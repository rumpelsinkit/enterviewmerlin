package co.com.service.stock;

import java.util.ArrayList;
import java.util.List;

import co.com.service.entities.Bici;

public class GenerateStock {
	private static int id=0;
	
	public static List<Bici> bicis;
	
	public GenerateStock() {
		createStock();
	}
	
	public void createStock() {
		
		Bici bici1 = new Bici(getId(),"Frq","S",2,"26","montana",25000);
		Bici bici2 = new Bici(getId(),"Frq","M",2,"27","montana",240);
		Bici bici3 = new Bici(getId(),"Frq","L",2,"29","montana",240);
		Bici bici4 = new Bici(getId(),"Frq","XL",2,"26","ruta",2500);
		Bici bici5 = new Bici(getId(),"Frq","S",2,"26","recreativa",250);
		bicis = new ArrayList<Bici>();
		bicis.add(bici1);
		bicis.add(bici2);
		bicis.add(bici3);
		bicis.add(bici4);
		bicis.add(bici5);
		
	}
	public static int getId() {
		id+=1;
		return id;
	}
	
	

}
