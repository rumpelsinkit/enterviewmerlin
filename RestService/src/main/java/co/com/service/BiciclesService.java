package co.com.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.service.entities.Bici;
import co.com.service.logic.ConsolidateData;
import co.com.service.stock.GenerateStock;

@RestController
public class BiciclesService {
	private ConsolidateData facade;
	
	public BiciclesService() {
		GenerateStock stok = new GenerateStock();
		facade = new ConsolidateData();
	}

	@RequestMapping(method=RequestMethod.GET)
	public List<Bici> getBicicles() {
		List<Bici> response= new ArrayList<Bici>();
		response = facade.queryData();	
		return response;
	}
	@RequestMapping(method=RequestMethod.PUT)
	public void updateBici(Bici bici) {
		
	}
	
	@RequestMapping(method=RequestMethod.DELETE)
	public void deleteBici(Bici bici) {
		
	}
	@RequestMapping(method=RequestMethod.POST)
	public void createBici(Bici bici) {
		
	}
}
