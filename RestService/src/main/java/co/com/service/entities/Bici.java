package co.com.service.entities;

public class Bici {
	public int id;
	public String model;
	public String frame;
	public int rin;
	public String size;
	public String type;
	public int price;
	
	public Bici(int id, String model,String frame,int rin,String size, String type,int price) {
		this.id = id;
		this.model = model;
		this.frame = frame;
		this.rin = rin;
		this.size = size;
		this.type = type;
		this.price = price;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getFrame() {
		return frame;
	}
	public void setFrame(String frame) {
		this.frame = frame;
	}
	public int getRin() {
		return rin;
	}
	public void setRin(int rin) {
		this.rin = rin;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	

}
