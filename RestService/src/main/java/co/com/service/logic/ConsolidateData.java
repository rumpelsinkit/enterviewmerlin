package co.com.service.logic;

import java.util.ArrayList;
import java.util.List;

import co.com.service.entities.Bici;
import co.com.service.stock.BiciOperations;

public class ConsolidateData {
	private BiciOperations operations;
	
	public ConsolidateData() {
		operations = new BiciOperations();
	}

	public List<Bici> queryData(){
		List<Bici> listBicis= new ArrayList<Bici>();
		listBicis = operations.getBicis();
		return listBicis;
	}
}
